# Udemy-Docker-Course
 Course by Stephen Grider
[https://www.udemy.com/docker-and-kubernetes-the-complete-guide](https://www.udemy.com/docker-and-kubernetes-the-complete-guide)


1. [section 1](1)
2. [section 2](2)


#Sections 

##1  Setup

Section one deals with the setup of Docker and does not have any containers

##2 Manipulating Containers with the Docker Client

Code contained in branch 2-manipulating-containers
