# 4 Making Real World Project With Docker

## Use Nodejs Base Container

- Use a base package suitable to the environment you wish to create.
- Alpine tags are the most stripped down with only the bare essentials.
- Alpine images are the most efficient to start with.

## Set a working directory
- Use WORKDIR to set a working directory on the target container.
- This will be the default directory to copy files to and
and the starting directory for shell interaction.

## Upload Local Package Files To Docker Container
-  Upload the local pakckage manager files first.  
-  In future updates to the image, this will ensure the cache runs
as far as yarn/nom/composer/gulp (etc).

## Install And Use Yarn
- Yarn (or composer, npm or other package management/pre-build).
- This will only run if there has been an update to the package files.

## Upload application files
- the files specific to the application can now be uploaded.
- These files are likely to be changed the most frequently and
should not have any impact on the technology stack in use.
- Uploading these last will keep the majority of the image 
build done from the cache, with only a few actual changes
in the app files.

## Use Port Forwarding In Docker Build Command

- This is not part of the Dockerfile scripting.
It is carreid out as part of the command line, at 
this point in the course.