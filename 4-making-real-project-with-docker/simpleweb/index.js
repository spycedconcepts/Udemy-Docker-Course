const express = require('express');

const app = express();

app.get('/', (req, res) => {
  res.send('Hi There.  What\'s up?');
});

app.listen(8080, () => {
  console.log('Listening out on port 8080');
});