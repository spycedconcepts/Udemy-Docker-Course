# 5 Docker Compose With Multiple Local Containers


##  docker-compose.yml

yaml file used to define services (containers), make them available to each other and map ports


## docker-compose commands:
```
docker-compose up 
```
- looks at docker-compose.yml, parses it into docker instructions. It starts up services, maps ports and sets up networking
---

```
docker-compose up --build
```
- same as `docker-compose up` but forces the build on an image.
---
---

```
docker-compose up -d
```
- Launches docker services in the background
---

```
docker-compose down
```
- Shut down all containers for docker service
---

## docker-compose "maintenance"

- "no" : never attempt to restart service (default) - (remember quotes)
- always : always restart service
- on-failure : restart service with error code except code 0
- unless-stopped : restart service unless we, as devs, forcibly stop the service.

- Note that each service will require a restart definitiion if default is not being used.
