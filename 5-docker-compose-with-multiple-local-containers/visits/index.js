const express = require('express');
const redis = require('redis');
const process = require('process');

const app = express();
const client = redis.createClient({
  host: 'redis-server',
  port: 6379
});
client.set('visits', 0);

app.get('/', (req, res) => {
  //process.exit(0); //arbitrary forced crash
  client.get('visits', (err, visits) => {
    res
    .status(200)
    .send(`Number of Visits is ${visits}`);
    client.set('visits', parseInt(visits) + 1 );
  });
});

app.listen(8081, () => {
  console.log("App listening on port 8081");
});