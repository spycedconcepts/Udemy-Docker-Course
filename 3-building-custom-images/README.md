# 3 Building Custom Images

## Creating Dockerfile
- create folder to contain the dockerfile and any other resources.
- within the new folder, create file with the precise named "Dockerfile".
- note filename has capital D and no extension.
- basic structure as follows (in comments)

```docker
# Use an existing docker image as a base.


# Download and install depency/dependencies.


# Tell the imate what to do when it starts as a container.


```
---

##  Getting the base image.0

---

## building custom docker image

from the image folder run 
`docker build .`

output:

```
$ docker build .
Sending build context to Docker daemon  2.048kB
Step 1/3 : FROM alpine
latest: Pulling from library/alpine
e7c96db7181b: Pull complete
Digest: sha256:769fddc7cc2f0a1c35abb2f91432e8beecf83916c421420e6a6da9f8975464b6
Status: Downloaded newer image for alpine:latest
 ---> 055936d39205
Step 2/3 : RUN apk add --update redis
 ---> Running in 9688caf3635f
fetch http://dl-cdn.alpinelinux.org/alpine/v3.9/main/x86_64/APKINDEX.tar.gz
fetch http://dl-cdn.alpinelinux.org/alpine/v3.9/community/x86_64/APKINDEX.tar.gz
(1/1) Installing redis (4.0.12-r0)
Executing redis-4.0.12-r0.pre-install
Executing redis-4.0.12-r0.post-install
Executing busybox-1.29.3-r10.trigger
OK: 7 MiB in 15 packages
Removing intermediate container 9688caf3635f
 ---> e15bf95fa598
Step 3/3 : CMD ["redis-server"]
 ---> Running in 4c8b4c285cf3
Removing intermediate container 4c8b4c285cf3
 ---> 59e18c9ddf66
Successfully built 59e18c9ddf66
SECURITY WARNING: You are building a Docker image from Windows against a non-Windows Docker host. All files and directories added to build context will have '-rwxr-xr-x' permissions. It is recommended to double check and reset permissions for sensitive files and directories.
```

- note that each step of the Dockerfile is reflected in the steps in the output, in this case 1/3, 2/3 and 3/3.
- note that setp 2/3 creates an intermediate container (with an ID) and removes this intermediate container before
going on to step 3/3.  The same happens in step 3/3 before a final container is built.
---

## run customer docker image

- using the id from the "succesfully built"
`docker run xxxxxx` where xxxxxx is the id from above.

- alternatively run `docker container ls` to get the names of the available containers or image id
- then `docker run yyyyyy` where yyyyyy is the docker contianer name.
- also note that `docker container ls` shows the container ID and the default command.

## Docker Cache

- adding additional config (ie `RUN apk add --update gcc`) and re-running the build
does not mean that the whole build is executed again.  The interim builds are kept 
in the docker cache and only those where changes will be made are rebuilt as 
interim containers.

The output of such an event is as follows:

```docker
$ docker build .
Sending build context to Docker daemon   2.56kB
Step 1/4 : FROM alpine
 ---> 055936d39205
Step 2/4 : RUN apk add --update redis
 ---> Using cache
 ---> e15bf95fa598
Step 3/4 : RUN apk add --update gcc
 ---> Running in 85a6be8795f5
fetch http://dl-cdn.alpinelinux.org/alpine/v3.9/main/x86_64/APKINDEX.tar.gz
fetch http://dl-cdn.alpinelinux.org/alpine/v3.9/community/x86_64/APKINDEX.tar.gz
(1/10) Installing binutils (2.31.1-r2)
(2/10) Installing gmp (6.1.2-r1)
(3/10) Installing isl (0.18-r0)
(4/10) Installing libgomp (8.3.0-r0)
(5/10) Installing libatomic (8.3.0-r0)
(6/10) Installing libgcc (8.3.0-r0)
(7/10) Installing mpfr3 (3.1.5-r1)
(8/10) Installing mpc1 (1.0.3-r1)
(9/10) Installing libstdc++ (8.3.0-r0)
(10/10) Installing gcc (8.3.0-r0)
Executing busybox-1.29.3-r10.trigger
OK: 93 MiB in 25 packages
Removing intermediate container 85a6be8795f5
 ---> 0eb665737184
Step 4/4 : CMD ["redis-server"]
 ---> Running in b76f4631b1ee
Removing intermediate container b76f4631b1ee
 ---> 2f517ed5a400
Successfully built 2f517ed5a400
SECURITY WARNING: You are building a Docker image from Windows against a non-Windows Docker host. All files and directories added to build context will have '-rwxr-xr-x' permissions. It is recommended to double check and reset permissions for sensitive files and directories.
```
---

## Adding a tag to the image to make `docker run` easier to do.

```
docker build -t dockerid/project:latest .
```
- the docker tag (dockerid/project:latest) is docker convention.
- dockerid is the same as your docker login name.
- project can be anything you want, but clearly should be descriptive
- latest is the version number.  Usually this will be latest, but you can use it for versioning.

When you use docker run, you can refer to the the image via the tag.  You can omit the version to
get the latest version or add the version to get a specific version.

``` 
docker run dockerid/project 
```
---

##  Manually creating an image out of a current container
- possible and occassionally useful but better to script
the build to guarantee consistent builds.

1.  create alpine container with terminal access to shell

``` 
docker run -it alpine sh
```

2.  Install redis-server using apk

```
apk add --update redis
```

3.  create new image from running container
- in a separate terminal ...
```
docker ps
```
- get the container id
```
docker commit -c 'CMD [" redis-server"]' 95e66b495579 
```
- create a new image using the container id
```
docker image ls
```
- get a list of the current images to see image id.
```
docker tag cf16973707a0 stuartlast/redis-server-demo-b:latest
```
-  assign a tag to the image specified by it's image id.
---

